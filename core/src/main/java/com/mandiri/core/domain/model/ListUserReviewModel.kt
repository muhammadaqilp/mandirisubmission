package com.mandiri.core.domain.model

import com.mandiri.core.data.source.remote.response.ListUserReviewResponse
import com.mandiri.core.util.Constants
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf

data class ListUserReviewModel(
    val review: List<Data>,
    val totalResult: Int
) {

    data class Data(
        val userName: String,
        val userReview: String,
        val userRating: String,
        val userAvatar: String
    )

    companion object {
        fun map(input: ListUserReviewResponse): Flow<ListUserReviewModel> {
            return flowOf(
                ListUserReviewModel(
                    totalResult = input.totalResults ?: 0,
                    review = input.results?.map {
                        Data(
                            userName = it?.author ?: "",
                            userReview = it?.content ?: "",
                            userRating = it?.authorDetails?.rating.toString(),
                            userAvatar = Constants.BASE_URL_IMAGE + it?.authorDetails?.avatarPath
                        )
                    } ?: listOf()
                )
            )
        }
    }

    enum class TYPE {
        LOADING, SUCCESS, EMPTY, ERROR
    }

}
