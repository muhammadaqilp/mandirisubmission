package com.mandiri.core.domain.repository

import com.mandiri.core.data.source.remote.Resource
import com.mandiri.core.domain.model.ListGenreModel
import kotlinx.coroutines.flow.Flow

interface IGenreRepository {
    fun getGenres(): Flow<Resource<ListGenreModel>>
}