package com.mandiri.core.domain.usecase.review

import com.mandiri.core.data.source.remote.Resource
import com.mandiri.core.domain.model.ListUserReviewModel
import com.mandiri.core.domain.repository.IReviewRepository
import javax.inject.Inject
import kotlinx.coroutines.flow.Flow

class ReviewInteractor @Inject constructor(
    private val reviewRepository: IReviewRepository
) : ReviewUseCase {
    override fun getListUserReview(movieId: Int, page: Int): Flow<Resource<ListUserReviewModel>> {
        return reviewRepository.getListUserReview(movieId, page)
    }
}