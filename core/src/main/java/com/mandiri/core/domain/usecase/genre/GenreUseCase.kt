package com.mandiri.core.domain.usecase.genre

import com.mandiri.core.data.source.remote.Resource
import com.mandiri.core.domain.model.ListGenreModel
import kotlinx.coroutines.flow.Flow

interface GenreUseCase {
    fun getGenres(): Flow<Resource<ListGenreModel>>
}