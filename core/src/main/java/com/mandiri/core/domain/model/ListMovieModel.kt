package com.mandiri.core.domain.model

import com.mandiri.core.data.source.remote.response.ListMovieResponse
import com.mandiri.core.util.Constants
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf

data class ListMovieModel(
    val movie: List<Data>
) {
    data class Data(
        val movieId: Int,
        val movieName: String,
        val posterUrl: String,
        val movieRating: Double,
        val movieReleaseDate: String
    )

    companion object {
        fun map(input: ListMovieResponse): Flow<ListMovieModel> {
            return flowOf(
                ListMovieModel(
                    movie = input.results?.map {
                        Data(
                            movieId = it?.id ?: 0,
                            movieName = it?.title ?: "",
                            posterUrl = (Constants.BASE_URL_IMAGE + it?.posterPath),
                            movieRating = it?.voteAverage ?: 0.0,
                            movieReleaseDate = it?.releaseDate ?: ""
                        )
                    } ?: listOf()
                )
            )
        }

    }

    enum class TYPE {
        LOADING, SUCCESS, EMPTY, ERROR
    }

}
