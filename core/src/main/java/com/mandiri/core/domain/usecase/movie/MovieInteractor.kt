package com.mandiri.core.domain.usecase.movie

import com.mandiri.core.data.source.remote.Resource
import com.mandiri.core.domain.model.ListMovieModel
import com.mandiri.core.domain.model.MovieDetailModel
import com.mandiri.core.domain.model.MovieVideoModel
import com.mandiri.core.domain.repository.IMovieRepository
import javax.inject.Inject
import kotlinx.coroutines.flow.Flow

class MovieInteractor @Inject constructor(
    private val movieRepository: IMovieRepository
): MovieUseCase {
    override fun getListMovieByGenreId(genreId: List<Long>, page: Int): Flow<Resource<ListMovieModel>> {
        val genre = genreId.joinToString(",")
        return movieRepository.getListMovieByGenreId(genre, page)
    }

    override fun getMovieDetail(movieId: Int): Flow<Resource<MovieDetailModel>> {
        return movieRepository.getMovieDetail(movieId)
    }

    override fun getMovieVideo(movieId: Int): Flow<Resource<MovieVideoModel>> {
        return movieRepository.getMovieVideo(movieId)
    }
}