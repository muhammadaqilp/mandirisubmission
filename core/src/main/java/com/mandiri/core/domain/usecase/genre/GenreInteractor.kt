package com.mandiri.core.domain.usecase.genre

import com.mandiri.core.data.source.remote.Resource
import com.mandiri.core.domain.model.ListGenreModel
import com.mandiri.core.domain.repository.IGenreRepository
import javax.inject.Inject
import kotlinx.coroutines.flow.Flow

class GenreInteractor @Inject constructor(
    private val genreRepository: IGenreRepository
) : GenreUseCase {
    override fun getGenres(): Flow<Resource<ListGenreModel>> {
        return genreRepository.getGenres()
    }

}