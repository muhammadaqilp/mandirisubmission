package com.mandiri.core.domain.usecase.movie

import com.mandiri.core.data.source.remote.Resource
import com.mandiri.core.domain.model.ListMovieModel
import com.mandiri.core.domain.model.MovieDetailModel
import com.mandiri.core.domain.model.MovieVideoModel
import kotlinx.coroutines.flow.Flow

interface MovieUseCase {
    fun getListMovieByGenreId(genreId: List<Long>, page: Int): Flow<Resource<ListMovieModel>>
    fun getMovieDetail(movieId: Int): Flow<Resource<MovieDetailModel>>
    fun getMovieVideo(movieId: Int): Flow<Resource<MovieVideoModel>>
}