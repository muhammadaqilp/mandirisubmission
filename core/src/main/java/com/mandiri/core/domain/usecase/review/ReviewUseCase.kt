package com.mandiri.core.domain.usecase.review

import com.mandiri.core.data.source.remote.Resource
import com.mandiri.core.domain.model.ListUserReviewModel
import kotlinx.coroutines.flow.Flow

interface ReviewUseCase {
    fun getListUserReview(movieId: Int, page: Int): Flow<Resource<ListUserReviewModel>>
}