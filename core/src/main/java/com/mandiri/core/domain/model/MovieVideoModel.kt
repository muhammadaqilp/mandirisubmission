package com.mandiri.core.domain.model

import com.mandiri.core.data.source.remote.response.ListVideoResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf

data class MovieVideoModel(
    val videoKey: String,
    val videoId: String
) {
    companion object {
        fun map(input: ListVideoResponse): Flow<MovieVideoModel> {
            val selectedData = input.results?.find {
                it?.name == OFFICIAL_TRAILER &&
                it.site == YOUTUBE &&
                it.type == TRAILER
            }
            return flowOf(
                MovieVideoModel(
                    videoKey = selectedData?.key ?: "",
                    videoId = selectedData?.id ?: ""
                )
            )
        }

        private const val OFFICIAL_TRAILER = "Official Trailer"
        private const val YOUTUBE = "YouTube"
        private const val TRAILER = "Trailer"
    }
}
