package com.mandiri.core.domain.repository

import com.mandiri.core.data.source.remote.Resource
import com.mandiri.core.domain.model.ListUserReviewModel
import kotlinx.coroutines.flow.Flow

interface IReviewRepository {
    fun getListUserReview(movieId: Int, page: Int): Flow<Resource<ListUserReviewModel>>
}