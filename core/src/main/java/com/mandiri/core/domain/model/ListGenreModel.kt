package com.mandiri.core.domain.model

import com.mandiri.core.data.source.remote.response.ListGenreResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf

data class ListGenreModel(
    val genre: List<Data>
) {

    data class Data(
        val genreName: String,
        val genreId: Long
    )

    companion object {
        fun map(input: ListGenreResponse): Flow<ListGenreModel> {
            return flowOf(
                ListGenreModel(
                    genre = input.genres?.map {
                        Data(
                            genreName = it.name ?: "",
                            genreId = it.id ?: 0
                        )
                    } ?: listOf()
                )
            )
        }
    }

    enum class TYPE {
        LOADING, SUCCESS, EMPTY, ERROR
    }

}
