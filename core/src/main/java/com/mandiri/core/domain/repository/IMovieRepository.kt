package com.mandiri.core.domain.repository

import com.mandiri.core.data.source.remote.Resource
import com.mandiri.core.domain.model.ListMovieModel
import com.mandiri.core.domain.model.MovieDetailModel
import com.mandiri.core.domain.model.MovieVideoModel
import kotlinx.coroutines.flow.Flow

interface IMovieRepository {
    fun getListMovieByGenreId(genreId: String, page: Int): Flow<Resource<ListMovieModel>>
    fun getMovieDetail(movieId: Int): Flow<Resource<MovieDetailModel>>
    fun getMovieVideo(movieId: Int): Flow<Resource<MovieVideoModel>>
}