package com.mandiri.core.domain.model

import com.mandiri.core.data.source.remote.response.MovieDetailResponse
import com.mandiri.core.util.Constants
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf

data class MovieDetailModel(
    val movieId: Int,
    val movieTitle: String,
    val movieBanner: String,
    val movieOverview: String,
    val movieGenre: List<String>,
    val movieReleaseDate: String,
    val movieRatings: Double,
    val movieDuration: Int,
    val movieHomepageUrl: String
) {

    companion object {
        fun map(input: MovieDetailResponse): Flow<MovieDetailModel> {
            return flowOf(
                MovieDetailModel(
                    movieId = input.id ?: 0,
                    movieTitle = input.originalTitle ?: "",
                    movieBanner = (Constants.BASE_URL_IMAGE + input.backdropPath),
                    movieOverview = input.overview ?: "",
                    movieGenre = input.genres?.map { it?.name ?: "" } ?: mutableListOf(),
                    movieReleaseDate = input.releaseDate ?: "",
                    movieRatings = input.voteAverage ?: 0.0,
                    movieDuration = input.runtime ?: 0,
                    movieHomepageUrl = input.homepage ?: ""
                )
            )
        }
    }

}
