package com.mandiri.core.util

object Constants {
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val BASE_URL_IMAGE = "https://image.tmdb.org/t/p/original"
    const val API_KEY = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJkZDYxYzcwNmM3ZjU3NWZjNDFiMjFlNTM1N2RlYjkzZCIsInN1YiI6IjY1ZGY2MmE2NWY0YjczMDE4NWY4Y2I1MiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.KGgi1v9O6Di_Kki9rz5Kh9vuSJUNOssq428BmsgtEDE"
    const val LANGUAGE = "en-US"
    const val SORT_BY = "popularity.desc"
    const val DEFAULT_ERROR_MESSAGE = "Something Went Wrong!"
}