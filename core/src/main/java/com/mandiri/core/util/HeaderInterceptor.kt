package com.mandiri.core.util

import okhttp3.Interceptor
import okhttp3.Response

class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()

        return chain.proceed(
            requestBuilder.apply {
                header(HEADER_NAME, "$HEADER_VALUE ${Constants.API_KEY}")
            }.build()
        )
    }

    companion object {
        private const val HEADER_NAME = "Authorization"
        private const val HEADER_VALUE = "Bearer"
    }
}