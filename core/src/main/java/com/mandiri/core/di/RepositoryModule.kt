package com.mandiri.core.di

import com.mandiri.core.data.repository.GenreRepository
import com.mandiri.core.data.repository.MovieRepository
import com.mandiri.core.data.repository.ReviewRepository
import com.mandiri.core.domain.repository.IGenreRepository
import com.mandiri.core.domain.repository.IMovieRepository
import com.mandiri.core.domain.repository.IReviewRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun getGenreRepository(repositoryImpl: GenreRepository): IGenreRepository

    @Binds
    abstract fun getMovieRepository(repositoryImpl: MovieRepository): IMovieRepository

    @Binds
    abstract fun getReviewRepository(repositoryImpl: ReviewRepository): IReviewRepository

}