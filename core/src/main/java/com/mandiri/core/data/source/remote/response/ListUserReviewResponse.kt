package com.mandiri.core.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class ListUserReviewResponse(
    @SerializedName("id") val id: Int?,
    @SerializedName("page") val page: Int?,
    @SerializedName("total_pages") val totalPages: Int?,
    @SerializedName("results") val results: List<ResultsItem?>?,
    @SerializedName("total_results") val totalResults: Int?
) {

    data class ResultsItem(
        @SerializedName("author_details") val authorDetails: AuthorDetails?,
        @SerializedName("updated_at") val updatedAt: String?,
        @SerializedName("author") val author: String?,
        @SerializedName("created_at") val createdAt: String?,
        @SerializedName("id") val id: String?,
        @SerializedName("content") val content: String?,
        @SerializedName("url") val url: String?
    )

    data class AuthorDetails(
        @SerializedName("avatar_path") val avatarPath: String?,
        @SerializedName("name") val name: String?,
        @SerializedName("rating") val rating: Int?,
        @SerializedName("username") val username: String?
    )

}