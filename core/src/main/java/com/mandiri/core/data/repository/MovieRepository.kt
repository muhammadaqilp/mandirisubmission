package com.mandiri.core.data.repository

import com.mandiri.core.data.source.remote.NetworkResource
import com.mandiri.core.data.source.remote.Resource
import com.mandiri.core.data.source.remote.network.ApiResponse
import com.mandiri.core.data.source.remote.network.MovieApi
import com.mandiri.core.data.source.remote.response.ListMovieResponse
import com.mandiri.core.data.source.remote.response.ListVideoResponse
import com.mandiri.core.data.source.remote.response.MovieDetailResponse
import com.mandiri.core.domain.model.ListMovieModel
import com.mandiri.core.domain.model.MovieDetailModel
import com.mandiri.core.domain.model.MovieVideoModel
import com.mandiri.core.domain.repository.IMovieRepository
import com.mandiri.core.util.Constants
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

@Singleton
class MovieRepository @Inject constructor(
    private val movieApi: MovieApi
) : IMovieRepository {
    override fun getListMovieByGenreId(genreId: String, page: Int): Flow<Resource<ListMovieModel>> =
        object : NetworkResource<ListMovieModel, ListMovieResponse>() {
            override fun mapResponseToModel(data: ListMovieResponse): Flow<ListMovieModel> {
                return ListMovieModel.map(data)
            }

            override suspend fun createCall(): Flow<ApiResponse<ListMovieResponse>> {
                return flow {
                    try {
                        val response = movieApi.getListMovies(page, genreId)
                        if (response.results?.isNotEmpty() == true) {
                            emit(ApiResponse.Success(response))
                        } else {
                            emit(ApiResponse.Empty)
                        }
                    } catch (e: Exception) {
                        emit(ApiResponse.Error(e.message ?: Constants.DEFAULT_ERROR_MESSAGE))
                    }
                }.flowOn(Dispatchers.IO)
            }

        }.asFlow()

    override fun getMovieDetail(movieId: Int): Flow<Resource<MovieDetailModel>> =
        object : NetworkResource<MovieDetailModel, MovieDetailResponse>() {
            override fun mapResponseToModel(data: MovieDetailResponse): Flow<MovieDetailModel> {
                return MovieDetailModel.map(data)
            }

            override suspend fun createCall(): Flow<ApiResponse<MovieDetailResponse>> {
                return flow {
                    try {
                        val response = movieApi.getMovieDetail(movieId)
                        emit(ApiResponse.Success(response))
                    } catch (e: Exception) {
                        emit(ApiResponse.Error(e.message ?: Constants.DEFAULT_ERROR_MESSAGE))
                    }
                }.flowOn(Dispatchers.IO)
            }

        }.asFlow()

    override fun getMovieVideo(movieId: Int): Flow<Resource<MovieVideoModel>> =
        object : NetworkResource<MovieVideoModel, ListVideoResponse>() {
            override fun mapResponseToModel(data: ListVideoResponse): Flow<MovieVideoModel> {
                return MovieVideoModel.map(data)
            }

            override suspend fun createCall(): Flow<ApiResponse<ListVideoResponse>> {
                return flow {
                    try {
                        val response = movieApi.getMovieVideos(movieId)
                        if (response.results?.isNotEmpty() == true) {
                            emit(ApiResponse.Success(response))
                        } else {
                            emit(ApiResponse.Empty)
                        }
                    } catch (e: Exception) {
                        emit(ApiResponse.Error(e.message ?: Constants.DEFAULT_ERROR_MESSAGE))
                    }
                }.flowOn(Dispatchers.IO)
            }

        }.asFlow()

}