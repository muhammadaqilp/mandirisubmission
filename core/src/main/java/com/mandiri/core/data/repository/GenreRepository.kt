package com.mandiri.core.data.repository

import com.mandiri.core.data.source.remote.NetworkResource
import com.mandiri.core.data.source.remote.Resource
import com.mandiri.core.data.source.remote.network.ApiResponse
import com.mandiri.core.data.source.remote.network.MovieApi
import com.mandiri.core.data.source.remote.response.ListGenreResponse
import com.mandiri.core.domain.model.ListGenreModel
import com.mandiri.core.domain.repository.IGenreRepository
import com.mandiri.core.util.Constants
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

@Singleton
class GenreRepository @Inject constructor(
    private val movieApi: MovieApi
) : IGenreRepository {
    override fun getGenres(): Flow<Resource<ListGenreModel>> =
        object : NetworkResource<ListGenreModel, ListGenreResponse>() {

            override fun mapResponseToModel(data: ListGenreResponse): Flow<ListGenreModel> =
                ListGenreModel.map(data)

            override suspend fun createCall(): Flow<ApiResponse<ListGenreResponse>> {
                return flow {
                    try {
                        val response = movieApi.getListGenre()
                        if (response.genres?.isNotEmpty() == true) {
                            emit(ApiResponse.Success(response))
                        } else {
                            emit(ApiResponse.Empty)
                        }
                    } catch (e: Exception) {
                        emit(ApiResponse.Error(e.message ?: Constants.DEFAULT_ERROR_MESSAGE))
                    }
                }.flowOn(Dispatchers.IO)
            }

        }.asFlow()

}