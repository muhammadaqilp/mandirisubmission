package com.mandiri.core.data.repository

import com.mandiri.core.data.source.remote.NetworkResource
import com.mandiri.core.data.source.remote.Resource
import com.mandiri.core.data.source.remote.network.ApiResponse
import com.mandiri.core.data.source.remote.network.MovieApi
import com.mandiri.core.data.source.remote.response.ListUserReviewResponse
import com.mandiri.core.domain.model.ListUserReviewModel
import com.mandiri.core.domain.repository.IReviewRepository
import com.mandiri.core.util.Constants
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

@Singleton
class ReviewRepository @Inject constructor(
    private val movieApi: MovieApi
) : IReviewRepository {
    override fun getListUserReview(movieId: Int, page: Int): Flow<Resource<ListUserReviewModel>> =
        object : NetworkResource<ListUserReviewModel, ListUserReviewResponse>() {
            override fun mapResponseToModel(data: ListUserReviewResponse): Flow<ListUserReviewModel> {
                return ListUserReviewModel.map(data)
            }

            override suspend fun createCall(): Flow<ApiResponse<ListUserReviewResponse>> {
                return flow {
                    try {
                        val response = movieApi.getUserReviewByMovieId(movieId, page)
                        if (response.results?.isNotEmpty() == true) {
                            emit(ApiResponse.Success(response))
                        } else {
                            emit(ApiResponse.Empty)
                        }
                    } catch (e: Exception) {
                        emit(ApiResponse.Error(e.message ?: Constants.DEFAULT_ERROR_MESSAGE))
                    }
                }.flowOn(Dispatchers.IO)
            }

        }.asFlow()
}