package com.mandiri.core.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class ListGenreResponse(
    @SerializedName("genres") val genres: List<Data>?
) {
    data class Data(
        @SerializedName("id") val id: Long?,
        @SerializedName("name") val name: String?
    )
}