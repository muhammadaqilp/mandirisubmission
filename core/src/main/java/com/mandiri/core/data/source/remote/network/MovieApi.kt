package com.mandiri.core.data.source.remote.network

import com.mandiri.core.data.source.remote.response.ListGenreResponse
import com.mandiri.core.data.source.remote.response.ListMovieResponse
import com.mandiri.core.data.source.remote.response.ListUserReviewResponse
import com.mandiri.core.data.source.remote.response.ListVideoResponse
import com.mandiri.core.data.source.remote.response.MovieDetailResponse
import com.mandiri.core.util.Constants
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieApi {

    /**
     * @see https://developer.themoviedb.org/reference/genre-movie-list
     */
    @GET("genre/movie/list")
    suspend fun getListGenre(@Query("language") language: String = Constants.LANGUAGE): ListGenreResponse

    /**
     * @see https://developer.themoviedb.org/reference/discover-movie
     */
    @GET("discover/movie")
    suspend fun getListMovies(
        @Query("page") page: Int,
        @Query("with_genres") genre: String,
        @Query("include_adult") includeAdult: Boolean = false,
        @Query("include_video") includeVideo: Boolean = false,
        @Query("sort_by") sortBy: String = Constants.SORT_BY,
        @Query("language") language: String = Constants.LANGUAGE
    ): ListMovieResponse

    /**
     * @see https://developer.themoviedb.org/reference/movie-details
     */
    @GET("movie/{movie_id}")
    suspend fun getMovieDetail(
        @Path("movie_id") movieId: Int,
        @Query("language") language: String = Constants.LANGUAGE
    ): MovieDetailResponse

    /**
     * @see https://developer.themoviedb.org/reference/movie-reviews
     */
    @GET("movie/{movie_id}/reviews")
    suspend fun getUserReviewByMovieId(
        @Path("movie_id") movieId: Int,
        @Query("page") page: Int,
        @Query("language") language: String = Constants.LANGUAGE
    ): ListUserReviewResponse

    /**
     * @see https://developer.themoviedb.org/reference/movie-videos
     */
    @GET("movie/{movie_id}/videos")
    suspend fun getMovieVideos(@Path("movie_id") movieId: Int): ListVideoResponse

}