plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    kotlin("kapt")
    id("com.google.dagger.hilt.android")
}
apply("../shared_dependencies.gradle")
android {
    namespace = "com.mandiri.core"
    compileSdk = Version.CompileSdk

    defaultConfig {
        minSdk = Version.MinSdk

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = Version.Kotlin.JvmTarget
    }
    buildFeatures {
        buildConfig = true
    }
    kapt {
        correctErrorTypes = true
    }
}

dependencies {
    //Retrofit
    implementation(Network.Retrofit)
    implementation(Network.RetrofitGson)

    //Logging Interceptor
    implementation(Network.OkHttp3)
    implementation(Network.LoggingInterceptor)

    //Chucker
    debugImplementation(Network.ChuckerDebug)
    releaseImplementation(Network.ChuckerRelease)

    //Gson
    implementation(Network.Gson)
}