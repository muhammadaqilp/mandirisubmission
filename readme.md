# Movie Apps for Mandiri Submission

**Hi! I'm Aqil.**  This Movie Apps projects contains some features such as list movie genre to discover the movie, the movie detail including movie trailer also movie reviews.

**Tech Stack:**

* Kotlin for Programming Languages
* Coroutines Flow for Reactive Programming
* Hilt for Dependency Injection
* Clean Architecture for The Structure
* MVVM for The Design Pattern
* Modularization for Splitting Core Module (Data) and App (UI)
* Kotlin-DSL for Manages The Dependencies
* Retrofit, OkHttp, Gson, Chucker for Handling Networking and Logging
* Glide for Showing The Images
* Fast Adapter for Recycler View Adapter
* Shimmer for Loading Effect
* Handle Loading State, Empty State, Error State

## Screenshots 🎉
![Image](https://gitlab.com/muhammadaqilp/mandirisubmission/-/raw/master/screenshot/Screenshot%202024-03-08%20at%2012.58.40.png)
![Image](https://gitlab.com/muhammadaqilp/mandirisubmission/-/raw/master/screenshot/Screenshot%202024-03-08%20at%2012.59.14.png)
![Image](https://gitlab.com/muhammadaqilp/mandirisubmission/-/raw/master/screenshot/Screenshot%202024-03-08%20at%2012.59.26.png)
![Image](https://gitlab.com/muhammadaqilp/mandirisubmission/-/raw/master/screenshot/Screenshot%202024-03-08%20at%2013.00.31.png)

**Feel free to ask me anything. Thanks!**

***Created By Muhammad Aqil Pratama***