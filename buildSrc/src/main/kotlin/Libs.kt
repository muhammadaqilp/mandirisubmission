import org.gradle.api.artifacts.dsl.DependencyHandler

object Version {
    const val CompileSdk = 34
    const val TargetSdk = 34
    const val MinSdk = 24
    const val VersionCode = 1
    const val VersionName = "1.0.0"

    object Kotlin {
        const val JvmTarget = "17"
        const val KotlinCompiler = "1.4.3"
    }

    object Android {
        const val CoreKtx = "1.9.0"
        const val JetpackKtx = "2.7.0"
        const val Coroutines = "1.3.9"
        const val LiveDataKtx = "2.7.0"
        const val AppCompat = "1.6.1"
        const val RecyclerView = "1.3.2"
        const val ConstraintLayout = "2.1.4"
        const val CardView = "1.0.0"
        const val ActivityKtx = "1.8.2"
        const val FragmentKtx = "1.6.2"
    }

    object Libs {
        const val Retrofit = "2.9.0"
        const val OkHttp3 = "4.10.0"
        const val Gson = "2.10.1"
        const val Glide = "4.16.0"
        const val Hilt = "2.51"
        const val HiltCompiler = "1.2.0"
        const val Chucker = "4.0.0"
        const val FastAdapter = "5.7.0"
        const val Shimmer = "0.5.0"
        const val GoogleMaterial = "1.11.0"
    }

    object TestLibs {
        const val JUnit = "4.13.2"
        const val ExtJUnit = "1.1.5"
        const val Espresso = "3.5.1"
    }
}

object AndroidX {
    const val CoreKtx = "androidx.core:core-ktx:${Version.Android.CoreKtx}"
    const val LifecycleKtx = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Version.Android.JetpackKtx}"
    const val AppCompat = "androidx.appcompat:appcompat:${Version.Android.AppCompat}"
    const val RecyclerView = "androidx.recyclerview:recyclerview:${Version.Android.RecyclerView}"
    const val ConstraintLayout = "androidx.constraintlayout:constraintlayout:${Version.Android.ConstraintLayout}"
    const val CardView = "androidx.cardview:cardview:${Version.Android.CardView}"
    const val ActivityKtx = "androidx.activity:activity-ktx:${Version.Android.ActivityKtx}"
    const val FragmentKtx = "androidx.fragment:fragment-ktx:${Version.Android.FragmentKtx}"
}

object Network {
    const val Retrofit = "com.squareup.retrofit2:retrofit:${Version.Libs.Retrofit}"
    const val RetrofitGson = "com.squareup.retrofit2:converter-gson:${Version.Libs.Retrofit}"
    const val OkHttp3 = "com.squareup.okhttp3:okhttp:${Version.Libs.OkHttp3}"
    const val LoggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Version.Libs.OkHttp3}"
    const val Gson = "com.google.code.gson:gson:${Version.Libs.Gson}"
    const val ChuckerDebug = "com.github.chuckerteam.chucker:library:${Version.Libs.Chucker}"
    const val ChuckerRelease = "com.github.chuckerteam.chucker:library-no-op:${Version.Libs.Chucker}"
}

object DependencyInjection {
    const val Hilt = "com.google.dagger:hilt-android:${Version.Libs.Hilt}"
    const val HiltAndroidCompiler = "com.google.dagger:hilt-android-compiler:${Version.Libs.Hilt}"
    const val HiltCompiler = "androidx.hilt:hilt-compiler:${Version.Libs.HiltCompiler}"
}

object Coroutines {
    const val Coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Version.Android.Coroutines}"
    const val CoroutinesCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Version.Android.Coroutines}"
}

object Images {
    const val Glide = "com.github.bumptech.glide:glide:${Version.Libs.Glide}"
}

object Shimmer {
    const val Shimmer = "com.facebook.shimmer:shimmer:${Version.Libs.Shimmer}"
}

object Google {
    const val Material = "com.google.android.material:material:${Version.Libs.GoogleMaterial}"
}

object RecyclerView {
    const val FastAdapter = "com.mikepenz:fastadapter:${Version.Libs.FastAdapter}"
    const val FastAdapterBinding = "com.mikepenz:fastadapter-extensions-binding:${Version.Libs.FastAdapter}"
    const val FastAdapterDiff = "com.mikepenz:fastadapter-extensions-diff:${Version.Libs.FastAdapter}"
    const val FastAdapterUtils = "com.mikepenz:fastadapter-extensions-utils:${Version.Libs.FastAdapter}"
    const val FastAdapterScroll = "com.mikepenz:fastadapter-extensions-scroll:${Version.Libs.FastAdapter}"
}

object Test {
    const val JUnit = "unit:junit:${Version.TestLibs.JUnit}"
    const val JUnitExt = "androidx.test.ext:junit:${Version.TestLibs.ExtJUnit}"
    const val Espresso = "androidx.test.espresso:espresso-core:${Version.TestLibs.Espresso}"
}