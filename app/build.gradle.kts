plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    kotlin("kapt")
    id("com.google.dagger.hilt.android")
}
apply("../shared_dependencies.gradle")
android {
    namespace = "com.mandiri.movieapps"
    compileSdk = Version.CompileSdk

    defaultConfig {
        applicationId = "com.mandiri.movieapps"
        minSdk = Version.MinSdk
        targetSdk = Version.TargetSdk
        versionCode = Version.VersionCode
        versionName = Version.VersionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = Version.Kotlin.JvmTarget
    }
    buildFeatures {
        buildConfig = true
        viewBinding = true
    }
    kapt {
        correctErrorTypes = true
    }
}

dependencies {
    implementation(project(":core"))

    //android x
    implementation(AndroidX.ConstraintLayout)
    implementation(AndroidX.RecyclerView)
    implementation(AndroidX.CardView)
    implementation(AndroidX.ActivityKtx)
    implementation(AndroidX.FragmentKtx)

    //Glide
    implementation(Images.Glide)

    //Shimmer
    implementation(Shimmer.Shimmer)

    //material
    implementation(Google.Material)

    //fast adapter
    implementation(RecyclerView.FastAdapter)
    implementation(RecyclerView.FastAdapterBinding)
    implementation(RecyclerView.FastAdapterDiff)
    implementation(RecyclerView.FastAdapterUtils)
    implementation(RecyclerView.FastAdapterScroll)

    //testing
    testImplementation(Test.JUnit)
    androidTestImplementation(Test.JUnitExt)
    androidTestImplementation(Test.Espresso)
}