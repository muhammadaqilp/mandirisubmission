package com.mandiri.movieapps.di

import com.mandiri.core.domain.usecase.genre.GenreInteractor
import com.mandiri.core.domain.usecase.genre.GenreUseCase
import com.mandiri.core.domain.usecase.movie.MovieInteractor
import com.mandiri.core.domain.usecase.movie.MovieUseCase
import com.mandiri.core.domain.usecase.review.ReviewInteractor
import com.mandiri.core.domain.usecase.review.ReviewUseCase
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {

    @Binds
    abstract fun provideGenreInteractor(genreInteractor: GenreInteractor): GenreUseCase

    @Binds
    abstract fun provideMovieInteractor(movieInteractor: MovieInteractor): MovieUseCase

    @Binds
    abstract fun provideReviewInteractor(reviewInteractor: ReviewInteractor): ReviewUseCase

}