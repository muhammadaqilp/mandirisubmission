package com.mandiri.movieapps.ui.detail.review.item

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mandiri.core.domain.model.ListUserReviewModel
import com.mandiri.movieapps.databinding.ItemUserReviewLoadingBinding
import com.mandiri.movieapps.util.DiffUtil
import com.mikepenz.fastadapter.binding.AbstractBindingItem

class UserReviewLoadingItem: AbstractBindingItem<ItemUserReviewLoadingBinding>(), DiffUtil {
    override val type: Int = ListUserReviewModel.TYPE.LOADING.ordinal

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ItemUserReviewLoadingBinding {
        return ItemUserReviewLoadingBinding.inflate(inflater, parent, false)
    }

    override fun itemIdentifier(): Any = hashCode()

    override fun comparableContents(): List<Any> = listOf()

    override fun bindView(binding: ItemUserReviewLoadingBinding, payloads: List<Any>) {
        binding.apply {
            shimmer.startShimmer()
        }
    }
}