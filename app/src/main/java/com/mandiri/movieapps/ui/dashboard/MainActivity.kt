package com.mandiri.movieapps.ui.dashboard

import android.Manifest
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mandiri.core.data.source.remote.Resource
import com.mandiri.movieapps.base.BaseActivity
import com.mandiri.movieapps.databinding.ActivityMainBinding
import com.mandiri.movieapps.ui.dashboard.item.genre.GenreEmptyItem
import com.mandiri.movieapps.ui.dashboard.item.genre.GenreErrorItem
import com.mandiri.movieapps.ui.dashboard.item.genre.GenreItem
import com.mandiri.movieapps.ui.dashboard.item.genre.GenreLoadingItem
import com.mandiri.movieapps.ui.dashboard.item.movie.MovieEmptyItem
import com.mandiri.movieapps.ui.dashboard.item.movie.MovieErrorItem
import com.mandiri.movieapps.ui.dashboard.item.movie.MovieItem
import com.mandiri.movieapps.ui.dashboard.item.movie.MovieLoadingItem
import com.mandiri.movieapps.ui.detail.MovieDetailActivity
import com.mandiri.movieapps.util.UnspecifiedTypeItem
import com.mandiri.movieapps.util.performUpdates
import com.mandiri.movieapps.util.plusAssign
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.select.getSelectExtension
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {

    private val viewModel: MainViewModel by viewModels()

    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (!it) {
                showToast("Granted Permission!")
            }
        }

    private val genreAdapter: FastItemAdapter<UnspecifiedTypeItem> = FastItemAdapter()
    private val movieAdapter: FastItemAdapter<UnspecifiedTypeItem> = FastItemAdapter()
    private val genreList: MutableList<Long> = mutableListOf()
    private var page = 1
    private var pastVisiblesItems = 0
    private var visibleItemCount = 0
    private var totalItemCount = 0
    private var isShouldLoading = true

    override fun setLayout(inflater: LayoutInflater): ActivityMainBinding {
        return ActivityMainBinding.inflate(inflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
        }

        viewModel.getGenre()
        fetchMovies()

        initObserver()
        initRecyclerView()
    }

    private fun initObserver() {
        viewModel.genreList.observe(this) { model ->
            when (model) {
                is Resource.Loading -> {
                    val items = mutableListOf<UnspecifiedTypeItem>()
                    items.add(GenreLoadingItem())
                    genreAdapter.performUpdates(items)
                }

                is Resource.Success -> {
                    val items = mutableListOf<UnspecifiedTypeItem>()
                    model.data?.genre?.forEachIndexed { index, item ->
                        genreAdapter.getSelectExtension().apply {
                            items.add(GenreItem(item, {
                                select(index, true)
                                genreList.add(it)
                                fetchMovies()
                            }, {
                                deselect(index)
                                genreList.remove(it)
                                fetchMovies()
                            }))
                        }
                    }
                    genreAdapter.performUpdates(items)
                }

                is Resource.Empty -> {
                    val items = mutableListOf<UnspecifiedTypeItem>()
                    items.add(GenreEmptyItem())
                    genreAdapter.performUpdates(items)
                }

                is Resource.Error -> {
                    val items = mutableListOf<UnspecifiedTypeItem>()
                    items.add(GenreErrorItem())
                    genreAdapter.performUpdates(items)
                }
            }
        }

        viewModel.movie.observe(this) { model ->
            when (model) {
                is Resource.Loading -> {
                    if (page == 1) {
                        val items = mutableListOf<UnspecifiedTypeItem>()
                        items.add(MovieLoadingItem())
                        items.add(MovieLoadingItem())
                        movieAdapter.performUpdates(items)
                    } else {
                        showCircleLoading(true)
                    }
                }

                is Resource.Success -> {
                    val items = mutableListOf<UnspecifiedTypeItem>()
                    model.data?.movie?.forEach {
                        items.add(MovieItem(it) { movieId ->
                            MovieDetailActivity.start(context = this, movieId = movieId)
                        })
                    }
                    if (page == 1) {
                        movieAdapter.performUpdates(items)
                    }
                    else {
                        showCircleLoading(false)
                        movieAdapter.plusAssign(items, movieAdapter.itemCount)
                    }
                    isShouldLoading = true
                }

                is Resource.Empty -> {
                    if (page == 1) {
                        val items = mutableListOf<UnspecifiedTypeItem>()
                        items.add(MovieEmptyItem())
                        movieAdapter.performUpdates(items)
                    } else {
                        isShouldLoading = false
                        showCircleLoading(false)
                    }
                }

                is Resource.Error -> {
                    if (page == 1) {
                        val items = mutableListOf<UnspecifiedTypeItem>()
                        items.add(MovieErrorItem())
                        movieAdapter.performUpdates(items)
                    } else {
                        isShouldLoading = false
                        showCircleLoading(false)
                    }
                }
            }
        }
    }

    private fun fetchMovies() {
        viewModel.getMovie(genreList, page)
    }

    private fun initRecyclerView() {
        binding.apply {
            rvGenre.layoutManager =
                LinearLayoutManager(this@MainActivity, LinearLayoutManager.HORIZONTAL, false)
            rvGenre.adapter = genreAdapter

            val layoutManager = GridLayoutManager(this@MainActivity, 2)
            rvMovie.layoutManager = layoutManager
            rvMovie.adapter = movieAdapter

            rvMovie.addOnScrollListener(object: RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if (dy > 0) {
                        visibleItemCount = layoutManager.childCount
                        totalItemCount = layoutManager.itemCount
                        pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()

                        if (isShouldLoading) {
                            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                isShouldLoading = false
                                page++
                                fetchMovies()
                            }
                        }
                    }
                }
            })
        }
    }

    private fun showCircleLoading(state: Boolean) {
        binding.apply {
            progressBar.isVisible = state
        }
    }
}