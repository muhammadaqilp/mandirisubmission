package com.mandiri.movieapps.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mandiri.core.data.source.remote.Resource
import com.mandiri.core.domain.model.ListUserReviewModel
import com.mandiri.core.domain.model.MovieDetailModel
import com.mandiri.core.domain.model.MovieVideoModel
import com.mandiri.core.domain.usecase.movie.MovieUseCase
import com.mandiri.core.domain.usecase.review.ReviewUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.launch

@HiltViewModel
class MovieDetailViewModel @Inject constructor(
    private val movieUseCase: MovieUseCase,
    private val reviewUseCase: ReviewUseCase
) : ViewModel() {

    private val _movieDetail: MutableLiveData<Resource<MovieDetailModel>> = MutableLiveData()
    val movieDetail: LiveData<Resource<MovieDetailModel>> = _movieDetail

    private val _movieVideo: MutableLiveData<Resource<MovieVideoModel>> = MutableLiveData()
    val movieVideo: LiveData<Resource<MovieVideoModel>> = _movieVideo

    private val _userReview: MutableLiveData<Resource<ListUserReviewModel>> = MutableLiveData()
    val userReview: LiveData<Resource<ListUserReviewModel>> = _userReview

    fun getMovieDetail(movieId: Int) {
        viewModelScope.launch {
            movieUseCase.getMovieDetail(movieId).collect {
                _movieDetail.value = it
            }
        }
    }

    fun getMovieVideo(movieId: Int) {
        viewModelScope.launch {
            movieUseCase.getMovieVideo(movieId).collect {
                _movieVideo.value = it
            }
        }
    }

    fun getUserReview(movieId: Int, page: Int) {
        viewModelScope.launch {
            reviewUseCase.getListUserReview(movieId, page).collect {
                _userReview.value = it
            }
        }
    }

}