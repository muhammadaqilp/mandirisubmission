package com.mandiri.movieapps.ui.detail.review.item

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mandiri.core.domain.model.ListUserReviewModel
import com.mandiri.movieapps.R
import com.mandiri.movieapps.databinding.ItemUserReviewBinding
import com.mandiri.movieapps.util.DiffUtil
import com.mandiri.movieapps.util.toHtml
import com.mikepenz.fastadapter.binding.AbstractBindingItem

class UserReviewItem(
    private val model: ListUserReviewModel.Data
) : AbstractBindingItem<ItemUserReviewBinding>(), DiffUtil {
    override val type: Int = ListUserReviewModel.TYPE.SUCCESS.ordinal

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ItemUserReviewBinding {
        return ItemUserReviewBinding.inflate(inflater, parent, false)
    }

    override fun itemIdentifier(): Any = model.userName

    override fun comparableContents(): List<Any> = listOf(model.userName, model.userReview)

    override fun bindView(binding: ItemUserReviewBinding, payloads: List<Any>) {
        binding.apply {
            val context = root.context
            Glide.with(context)
                .load(model.userAvatar)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .placeholder(R.drawable.ic_person)
                .into(ivAvatar)
            tvUserName.text = model.userName
            tvRating.text =
                String.format(context.getString(R.string.movie_rating), model.userRating)
            tvUserReview.text = model.userReview.toHtml()
        }
    }
}