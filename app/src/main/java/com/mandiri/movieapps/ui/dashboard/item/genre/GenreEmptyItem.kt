package com.mandiri.movieapps.ui.dashboard.item.genre

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mandiri.core.domain.model.ListGenreModel
import com.mandiri.movieapps.databinding.ItemGenreEmptyBinding
import com.mandiri.movieapps.util.DiffUtil
import com.mikepenz.fastadapter.binding.AbstractBindingItem

class GenreEmptyItem: AbstractBindingItem<ItemGenreEmptyBinding>(), DiffUtil {
    override val type: Int = ListGenreModel.TYPE.EMPTY.ordinal

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ItemGenreEmptyBinding {
        return ItemGenreEmptyBinding.inflate(inflater, parent, false)
    }

    override fun itemIdentifier(): Any = hashCode()

    override fun comparableContents(): List<Any> = listOf()
}