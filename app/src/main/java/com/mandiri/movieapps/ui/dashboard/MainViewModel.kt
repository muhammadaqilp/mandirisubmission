package com.mandiri.movieapps.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mandiri.core.data.source.remote.Resource
import com.mandiri.core.domain.model.ListGenreModel
import com.mandiri.core.domain.model.ListMovieModel
import com.mandiri.core.domain.usecase.genre.GenreUseCase
import com.mandiri.core.domain.usecase.movie.MovieUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.launch

@HiltViewModel
class MainViewModel @Inject constructor(
    private val genreUseCase: GenreUseCase,
    private val movieUseCase: MovieUseCase
) : ViewModel() {

    private val _genreList: MutableLiveData<Resource<ListGenreModel>> = MutableLiveData()
    val genreList: LiveData<Resource<ListGenreModel>> = _genreList

    private val _movie: MutableLiveData<Resource<ListMovieModel>> = MutableLiveData()
    val movie: LiveData<Resource<ListMovieModel>> = _movie

    fun getGenre() {
        viewModelScope.launch {
            genreUseCase.getGenres().collect {
                _genreList.value = it
            }
        }
    }

    fun getMovie(genre: List<Long>, page: Int) {
        viewModelScope.launch {
            movieUseCase.getListMovieByGenreId(genre, page).collect {
                _movie.value = it
            }
        }
    }

}