package com.mandiri.movieapps.ui.dashboard.item.genre

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.mandiri.core.domain.model.ListGenreModel
import com.mandiri.movieapps.R
import com.mandiri.movieapps.databinding.ItemGenreBinding
import com.mandiri.movieapps.util.DiffUtil
import com.mikepenz.fastadapter.binding.AbstractBindingItem

class GenreItem(
    private val model: ListGenreModel.Data,
    private val onClick: (genreId: Long) -> Unit,
    private val onClickCancel: (genreId: Long) -> Unit
): AbstractBindingItem<ItemGenreBinding>(), DiffUtil {

    override val type: Int = ListGenreModel.TYPE.SUCCESS.ordinal

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ItemGenreBinding {
        return ItemGenreBinding.inflate(inflater, parent, false)
    }

    override fun itemIdentifier(): Any {
        return listOf(model.genreId)
    }

    override fun comparableContents(): List<Any> {
        return listOf(model.genreId, model.genreName)
    }

    override fun bindView(binding: ItemGenreBinding, payloads: List<Any>) {
        binding.apply {
            val context = root.context
            tvGenre.text = model.genreName
            if (isSelected) {
                root.backgroundTintList = context.getColorStateList(R.color.blue_light)
                tvGenre.setTextColor(ContextCompat.getColor(context, R.color.white))
            } else {
                root.backgroundTintList = context.getColorStateList(R.color.white)
                tvGenre.setTextColor(ContextCompat.getColor(context, R.color.black))
            }
            tvGenre.setOnClickListener {
                if (isSelected) {
                    onClickCancel.invoke(model.genreId)
                } else {
                    onClick.invoke(model.genreId)
                }
            }
        }
    }

}