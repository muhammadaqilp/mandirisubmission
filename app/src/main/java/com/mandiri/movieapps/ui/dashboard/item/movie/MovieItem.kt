package com.mandiri.movieapps.ui.dashboard.item.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mandiri.core.domain.model.ListMovieModel
import com.mandiri.movieapps.R
import com.mandiri.movieapps.databinding.ItemMovieBinding
import com.mandiri.movieapps.util.DiffUtil
import com.mikepenz.fastadapter.binding.AbstractBindingItem

data class MovieItem(
    private val model: ListMovieModel.Data,
    private val onClick: (movieId: Int) -> Unit
) : AbstractBindingItem<ItemMovieBinding>(), DiffUtil {

    override val type: Int = ListMovieModel.TYPE.SUCCESS.ordinal

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ItemMovieBinding {
        return ItemMovieBinding.inflate(inflater, parent, false)
    }

    override fun itemIdentifier(): Any = model.movieId

    override fun comparableContents(): List<Any> = listOf(model.movieId)

    override fun bindView(binding: ItemMovieBinding, payloads: List<Any>) {
        binding.apply {
            val context = root.context
            Glide.with(context)
                .load(model.posterUrl)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(ivPoster)
            tvTitle.text = model.movieName
            val rate = String.format("%.1f", model.movieRating)
            tvRating.text = String.format(context.getString(R.string.movie_rating), rate)
            tvReleaseDate.text = model.movieReleaseDate.take(4)

            layoutMovie.setOnClickListener {
                onClick.invoke(model.movieId)
            }
        }
    }

}