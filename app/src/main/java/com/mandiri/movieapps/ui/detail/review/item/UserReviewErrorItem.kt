package com.mandiri.movieapps.ui.detail.review.item

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mandiri.core.domain.model.ListUserReviewModel
import com.mandiri.movieapps.databinding.ItemUserReviewErrorBinding
import com.mandiri.movieapps.util.DiffUtil
import com.mikepenz.fastadapter.binding.AbstractBindingItem

class UserReviewErrorItem : AbstractBindingItem<ItemUserReviewErrorBinding>(), DiffUtil {
    override val type: Int = ListUserReviewModel.TYPE.ERROR.ordinal

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ItemUserReviewErrorBinding {
        return ItemUserReviewErrorBinding.inflate(inflater, parent, false)
    }

    override fun itemIdentifier(): Any = hashCode()

    override fun comparableContents(): List<Any> = listOf()

}