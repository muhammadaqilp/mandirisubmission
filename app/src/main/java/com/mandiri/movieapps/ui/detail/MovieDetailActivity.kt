package com.mandiri.movieapps.ui.detail

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.webkit.WebSettings
import android.webkit.WebViewClient
import androidx.activity.viewModels
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mandiri.core.data.source.remote.Resource
import com.mandiri.core.domain.model.MovieDetailModel
import com.mandiri.movieapps.R
import com.mandiri.movieapps.base.BaseActivity
import com.mandiri.movieapps.databinding.ActivityMovieDetailBinding
import com.mandiri.movieapps.ui.detail.review.UserReviewDialog
import com.mandiri.movieapps.util.getHTMLData
import com.mandiri.movieapps.util.openUrlInBrowser
import com.mandiri.movieapps.util.setStatusBarTransparent
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieDetailActivity : BaseActivity<ActivityMovieDetailBinding>() {

    private val viewModel: MovieDetailViewModel by viewModels()
    private val movieId by lazy { intent?.getIntExtra(MOVIE_ID, 0) ?: 0 }
    private var movieUrl = ""

    override fun setLayout(inflater: LayoutInflater): ActivityMovieDetailBinding {
        return ActivityMovieDetailBinding.inflate(inflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBarTransparent()
        initListener()
        initObserver()
        viewModel.getMovieDetail(movieId)
        viewModel.getMovieVideo(movieId)
    }

    private fun initListener() {
        binding.apply {
            ivBack.setOnClickListener {
                finish()
            }

            tvVisitMovieHomepage.setOnClickListener {
                if (movieUrl.isNotEmpty()) {
                    openUrlInBrowser(movieUrl)
                }
            }

            tvSeeReviews.setOnClickListener {
                UserReviewDialog.newInstance(movieId)
                    .showDialog(this@MovieDetailActivity)
            }
        }
    }

    private fun initObserver() {
        viewModel.movieDetail.observe(this) { model ->
            when (model) {
                is Resource.Loading -> {
                    showLoading(true)
                }

                is Resource.Success -> {
                    showLoading(false)
                    model.data?.let { setupUiLayout(it) }
                }

                else -> {
                    showLoading(false)
                    model.message?.let { showToast(it) }
                }
            }
        }

        viewModel.movieVideo.observe(this) { model ->
            when (model) {
                is Resource.Loading -> {}

                is Resource.Success -> {
                    model.data?.let { setupWebView(it.videoKey) }
                }

                else -> {
                    binding.labelTrailer.isVisible = false
                    binding.webview.isVisible = false
                    model.message?.let { showToast(it) }
                }
            }
        }
    }

    private fun setupUiLayout(data: MovieDetailModel) {
        binding.apply {
            Glide.with(this@MovieDetailActivity)
                .load(data.movieBanner)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(ivBanner)
            tvTitle.text = data.movieTitle
            tvOverview.text = data.movieOverview
            tvGenre.text = data.movieGenre.joinToString(", ")
            tvYear.text = data.movieReleaseDate.take(4)
            val rate = String.format("%.1f", data.movieRatings)
            tvRating.text = String.format(getString(R.string.movie_rating), rate)
            tvDuration.text = String.format(
                getString(R.string.duration_minutes, data.movieDuration)
            )
            tvVisitMovieHomepage.isEnabled = true
            tvVisitMovieHomepage.isVisible = data.movieHomepageUrl.isNotEmpty()
            movieUrl = data.movieHomepageUrl
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setupWebView(videoKey: String) {
        binding.webview.apply {
            settings.domStorageEnabled = true
            settings.loadsImagesAutomatically = true
            settings.javaScriptEnabled = true
            settings.useWideViewPort = true
            settings.loadWithOverviewMode = true
            settings.javaScriptCanOpenWindowsAutomatically = true
            settings.setSupportMultipleWindows(true)
            settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW

            webViewClient = WebViewClient()

            loadDataWithBaseURL(
                "https://www.youtube.com",
                getHTMLData(videoKey),
                "text/html",
                "UTF-8",
                null
            )
        }
    }

    private fun showLoading(state: Boolean) {
        binding.apply {
            if (state) {
                shimmerBanner.startShimmer()
                shimmerTitle.startShimmer()
                shimmerOverview.startShimmer()
                shimmerGenre.startShimmer()
                shimmerYear.startShimmer()
                shimmerRating.startShimmer()
                shimmerDuration.startShimmer()
            } else {
                shimmerBanner.stopShimmer()
                shimmerTitle.stopShimmer()
                shimmerOverview.stopShimmer()
                shimmerGenre.stopShimmer()
                shimmerYear.stopShimmer()
                shimmerRating.stopShimmer()
                shimmerDuration.stopShimmer()
                shimmerBanner.isVisible = false
                shimmerOverview.isVisible = false
                shimmerTitle.isVisible = false
                shimmerGenre.isVisible = false
                shimmerYear.isVisible = false
                shimmerRating.isVisible = false
                shimmerDuration.isVisible = false
            }
        }
    }

    companion object {
        @JvmStatic
        fun start(context: Context, movieId: Int) {
            val starter = Intent(context, MovieDetailActivity::class.java)
                .putExtra(MOVIE_ID, movieId)
            context.startActivity(starter)
        }

        private const val MOVIE_ID = "movie_id"
    }
}