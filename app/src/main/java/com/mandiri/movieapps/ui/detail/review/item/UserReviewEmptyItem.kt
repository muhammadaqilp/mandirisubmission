package com.mandiri.movieapps.ui.detail.review.item

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mandiri.core.domain.model.ListUserReviewModel
import com.mandiri.movieapps.databinding.ItemUserReviewEmptyBinding
import com.mandiri.movieapps.util.DiffUtil
import com.mikepenz.fastadapter.binding.AbstractBindingItem

class UserReviewEmptyItem : AbstractBindingItem<ItemUserReviewEmptyBinding>(), DiffUtil {
    override val type: Int = ListUserReviewModel.TYPE.EMPTY.ordinal

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ItemUserReviewEmptyBinding {
        return ItemUserReviewEmptyBinding.inflate(inflater, parent, false)
    }

    override fun itemIdentifier(): Any = hashCode()

    override fun comparableContents(): List<Any> = listOf()

}