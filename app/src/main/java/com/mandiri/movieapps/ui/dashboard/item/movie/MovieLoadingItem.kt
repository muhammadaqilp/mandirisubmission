package com.mandiri.movieapps.ui.dashboard.item.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mandiri.core.domain.model.ListMovieModel
import com.mandiri.movieapps.databinding.ItemMovieLoadingBinding
import com.mandiri.movieapps.util.DiffUtil
import com.mikepenz.fastadapter.binding.AbstractBindingItem

class MovieLoadingItem : AbstractBindingItem<ItemMovieLoadingBinding>(), DiffUtil {
    override val type: Int = ListMovieModel.TYPE.LOADING.ordinal

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ItemMovieLoadingBinding {
        return ItemMovieLoadingBinding.inflate(inflater, parent, false)
    }

    override fun itemIdentifier(): Any = hashCode()

    override fun comparableContents(): List<Any> = listOf()

    override fun bindView(binding: ItemMovieLoadingBinding, payloads: List<Any>) {
        binding.apply {
            shimmer.startShimmer()
        }
    }
}