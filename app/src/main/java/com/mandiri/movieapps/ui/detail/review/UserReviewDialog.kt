package com.mandiri.movieapps.ui.detail.review

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mandiri.core.data.source.remote.Resource
import com.mandiri.movieapps.R
import com.mandiri.movieapps.databinding.DialogUserReviewBinding
import com.mandiri.movieapps.ui.detail.MovieDetailViewModel
import com.mandiri.movieapps.ui.detail.review.item.UserReviewEmptyItem
import com.mandiri.movieapps.ui.detail.review.item.UserReviewErrorItem
import com.mandiri.movieapps.ui.detail.review.item.UserReviewItem
import com.mandiri.movieapps.ui.detail.review.item.UserReviewLoadingItem
import com.mandiri.movieapps.util.UnspecifiedTypeItem
import com.mandiri.movieapps.util.performUpdates
import com.mandiri.movieapps.util.plusAssign
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserReviewDialog : BottomSheetDialogFragment() {

    private lateinit var binding: DialogUserReviewBinding
    private val viewModel: MovieDetailViewModel by viewModels()
    private val reviewAdapter: FastItemAdapter<UnspecifiedTypeItem> = FastItemAdapter()
    private val movieId by lazy { arguments?.getInt(MOVIE_ID, 0) ?: 0 }
    private var page = 1
    private var totalResult = 0
    private var pastVisiblesItems = 0
    private var visibleItemCount = 0
    private var totalItemCount = 0
    private var isShouldLoading = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogFloatingRounded)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dialog?.let {
            it.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            it.requestWindowFeature(Window.FEATURE_NO_TITLE)
        }
        binding = DialogUserReviewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObserver()
        initAdapter()
        fetchUserReview()
    }

    private fun initObserver() {
        viewModel.userReview.observe(this) { model ->
            when (model) {
                is Resource.Loading -> {
                    if (page == 1) {
                        val items: MutableList<UnspecifiedTypeItem> = mutableListOf()
                        items.add(UserReviewLoadingItem())
                        items.add(UserReviewLoadingItem())
                        items.add(UserReviewLoadingItem())
                        reviewAdapter.performUpdates(items)
                    }
                }

                is Resource.Success -> {
                    val items: MutableList<UnspecifiedTypeItem> = mutableListOf()
                    model.data?.review?.forEach {
                        items.add(UserReviewItem(it))
                    }
                    totalResult = model.data?.totalResult ?: 0
                    if (page == 1) {
                        reviewAdapter.performUpdates(items)
                    }
                    else {
                        isShouldLoading = true
                        reviewAdapter.plusAssign(items, reviewAdapter.itemCount)
                    }
                }

                is Resource.Empty -> {
                    if (page == 1) {
                        val items: MutableList<UnspecifiedTypeItem> = mutableListOf()
                        items.add(UserReviewEmptyItem())
                        reviewAdapter.performUpdates(items)
                    } else {
                        isShouldLoading = false
                    }
                }

                is Resource.Error -> {
                    if (page == 1) {
                        val items: MutableList<UnspecifiedTypeItem> = mutableListOf()
                        items.add(UserReviewErrorItem())
                        reviewAdapter.performUpdates(items)
                    }
                    isShouldLoading = false
                    model.message?.let {
                        Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    private fun initAdapter() {
        binding.apply {
            val layoutManager = LinearLayoutManager(requireContext())
            rvReview.layoutManager = layoutManager
            rvReview.adapter = reviewAdapter
            rvReview.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if (dy > 0) {
                        visibleItemCount = layoutManager.childCount
                        totalItemCount = layoutManager.itemCount
                        pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()

                        if (isShouldLoading && reviewAdapter.itemCount < totalResult) {
                            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                isShouldLoading = false
                                page++
                                fetchUserReview()
                            }
                        }
                    }
                }
            })
        }
    }

    private fun fetchUserReview() {
        viewModel.getUserReview(movieId, page)
    }

    fun showDialog(context: Context?) {
        when {
            this.isAdded -> onStart()
            else -> {
                if (context != null && context is FragmentActivity) {
                    if (!context.isFinishing) {
                        val fragmentManager: FragmentManager = context.supportFragmentManager
                        val fragmentTransaction: FragmentTransaction =
                            fragmentManager.beginTransaction()
                        fragmentTransaction.add(this, UserReviewDialog::class.java.simpleName)
                        fragmentTransaction.commitAllowingStateLoss()
                        this.isCancelable = true
                    }
                }
            }
        }
    }

    companion object {
        fun newInstance(movieId: Int): UserReviewDialog {
            val fragment = UserReviewDialog()
            val bundle = Bundle()
            bundle.putInt(MOVIE_ID, movieId)
            fragment.arguments = bundle
            return fragment
        }

        private const val MOVIE_ID = "movie_id"
    }

}