package com.mandiri.movieapps.ui.dashboard.item.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mandiri.core.domain.model.ListMovieModel
import com.mandiri.movieapps.databinding.ItemMovieErrorBinding
import com.mandiri.movieapps.util.DiffUtil
import com.mikepenz.fastadapter.binding.AbstractBindingItem

class MovieErrorItem: AbstractBindingItem<ItemMovieErrorBinding>(), DiffUtil {
    override val type: Int = ListMovieModel.TYPE.ERROR.ordinal

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ItemMovieErrorBinding {
        return ItemMovieErrorBinding.inflate(inflater, parent, false)
    }

    override fun itemIdentifier(): Any = hashCode()

    override fun comparableContents(): List<Any> = listOf()
}