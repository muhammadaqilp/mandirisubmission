package com.mandiri.movieapps.ui.dashboard.item.genre

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mandiri.core.domain.model.ListGenreModel
import com.mandiri.movieapps.databinding.ItemGenreLoadingBinding
import com.mandiri.movieapps.util.DiffUtil
import com.mikepenz.fastadapter.binding.AbstractBindingItem

class GenreLoadingItem: AbstractBindingItem<ItemGenreLoadingBinding>(), DiffUtil {

    override val type: Int = ListGenreModel.TYPE.LOADING.ordinal

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ItemGenreLoadingBinding {
        return ItemGenreLoadingBinding.inflate(inflater, parent, false)
    }

    override fun itemIdentifier(): Any = hashCode()

    override fun comparableContents(): List<Any> = listOf()

    override fun bindView(binding: ItemGenreLoadingBinding, payloads: List<Any>) {
        binding.apply {
            shimmer.startShimmer()
        }
    }
}