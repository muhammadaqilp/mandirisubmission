package com.mandiri.movieapps.util

import com.mikepenz.fastadapter.IItem
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.diff.FastAdapterDiffUtil

typealias UnspecifiedTypeItem = IItem<*>

fun <Item : UnspecifiedTypeItem> FastItemAdapter<Item>.performUpdates(newItems: List<Item>) {
    val diffResult =
        FastAdapterDiffUtil.calculateDiff(this.itemAdapter, newItems, DiffableCallback())
    FastAdapterDiffUtil[this.itemAdapter] = diffResult
}

fun <Item : UnspecifiedTypeItem> FastItemAdapter<Item>.plusAssign(item: List<Item>, pos: Int) {
    add(item)
    notifyItemInserted(pos)
}