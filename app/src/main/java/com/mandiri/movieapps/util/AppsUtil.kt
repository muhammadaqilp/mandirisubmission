package com.mandiri.movieapps.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.text.Html
import android.text.Spanned
import android.view.WindowManager
import android.webkit.URLUtil
import android.widget.Toast
import androidx.core.text.HtmlCompat
import com.mandiri.movieapps.R

fun Context.openUrlInBrowser(url: String) {
    if (URLUtil.isValidUrl(url)) {
        Intent(Intent.ACTION_VIEW, Uri.parse(url)).apply { startActivity(this) }
    } else {
        Toast.makeText(this, getString(R.string.invalid_url), Toast.LENGTH_SHORT).show()
    }
}

fun Activity.setStatusBarTransparent() {
    window.setFlags(
        WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
        WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
    )
    window.statusBarColor = Color.TRANSPARENT
}

fun String.toHtml(): Spanned {
    return Html.fromHtml(this, HtmlCompat.FROM_HTML_MODE_LEGACY)
}

fun getHTMLData(videoId: String): String {
    return """
        <html>
            <body style="margin:0px;padding:0px;">
               <div id="player"></div>
                <script>
                    var player;
                    function onYouTubeIframeAPIReady() {
                        player = new YT.Player('player', {
                            height: '600',
                            width: '100%',
                            videoId: '$videoId',
                            playerVars: {
                                'playsinline': 1
                            },
                            events: {
                                'onReady': onPlayerReady
                            }
                        });
                    }
                    function onPlayerReady(event) {
                     player.playVideo();
                        // Player is ready
                    }
                    function seekTo(time) {
                        player.seekTo(time, true);
                    }
                      function playVideo() {
                        player.playVideo();
                    }
                    function pauseVideo() {
                        player.pauseVideo();
                    }
                </script>
                <script src="https://www.youtube.com/iframe_api"></script>
            </body>
        </html>
    """.trimIndent()
}